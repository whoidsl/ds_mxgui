#!/usr/bin/env python

# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os
import rospy
import rospkg
import datetime

from qt_gui.plugin import Plugin
from python_qt_binding.QtWidgets import *
from python_qt_binding.QtCore import pyqtSignal, Qt

from ds_mx_msgs.msg import MxEvent
from ds_mx_msgs.msg import MxEventLog
from ds_mx_msgs.msg import MxMissionStatus
from ds_mx_msgs.msg import MxSharedParams
from ds_mx_msgs.srv import MxUpdateSharedParams
from ds_mx_msgs.srv import MxUpdateSharedParamsRequest
from ds_core_msgs.msg import KeyString

class MxPyPlugin(Plugin):

    status_update = pyqtSignal(object)
    param_update = pyqtSignal(object)
    eventlog_update = pyqtSignal(object)

    def __init__ (self, context):
        super(MxPyPlugin, self).__init__(context)

        self.setObjectName("MxPyPlugin")
        self._init_widgets()
        context.add_widget(self._widget)

        self._status_subscription = rospy.Subscriber('/mxsim/mxsim/mx_state',
                                                     MxMissionStatus, self._mission_status_cb)
        self._shared_param_subscription = rospy.Subscriber('/mxsim/mxsim/shared_params',
                                                           MxSharedParams, self._shared_param_cb)
        self._eventlog_subscription = rospy.Subscriber('/mxsim/mxsim/eventlog',
                                                       MxEventLog, self._eventlog_cb)
        self._parameter_service = rospy.ServiceProxy('/mxsim/mxsim/update_shared_params', MxUpdateSharedParams)
        self._event_publisher = rospy.Publisher('/mxsim/mxsim/mx_event_in', MxEvent, queue_size=10)

        self.status_update.connect(self._update_status_widgets)
        self.param_update.connect(self._update_param_widgets)
        self.eventlog_update.connect(self._update_eventlog_widgets)

    def shutdown_plugin(self):
        self._status_subscription.unregister()
        self._shared_param_subscription.unregister()
        self._parameter_service.close()

    def _init_widgets(self):
        pass

    def _mission_status_cb(self, msg):
        self.status_update.emit(msg)

    def _shared_param_cb(self, msg):
        self.param_update.emit(msg)

    def _eventlog_cb(self, msg):
        self.eventlog_update.emit(msg)

    def _update_status_widgets(self, msg):
        self._status_widget.setRowCount(len(msg.active_tasks))

        for i in range(0, len(msg.active_tasks)):
            self._status_widget.setItem(i, 0, QTableWidgetItem(msg.active_tasks[i].name))
            self._status_widget.setItem(i, 1, QTableWidgetItem(msg.active_tasks[i].type))
            if msg.active_tasks[i].timeout.to_sec() == 0:
                self._status_widget.setItem(i, 2, QTableWidgetItem('---'))
            else:
                self._status_widget.setItem(i, 2, QTableWidgetItem(str(
                    datetime.timedelta(seconds=msg.active_tasks[i].timeout_left.to_sec()))))


    def _update_param_widgets(self, msg):
        self._param_widget.setRowCount(len(msg.values))

        old_block_state = self._param_widget.blockSignals(True)
        for i in range(0, len(msg.values)):
            if i==self._param_widget.currentRow():
                continue
            self._param_widget.setItem(i,0,QTableWidgetItem(msg.values[i].key))
            self._param_widget.setItem(i,1,QTableWidgetItem(msg.values[i].value))
        self._param_widget.blockSignals(old_block_state)

    def _update_eventlog_widgets(self, msg):
        for i in range(self._eventlog_widget.rowCount(), 0, -1):
            for j in range(0,self._eventlog_widget.columnCount()):
                if self._eventlog_widget.item(i-1,j):
                    self._eventlog_widget.setItem(i,j,QTableWidgetItem(
                        self._eventlog_widget.item(i-1,j).text()))

        dt = datetime.datetime.utcfromtimestamp(msg.header.stamp.to_sec())

        self._eventlog_widget.setItem(0,0,QTableWidgetItem(dt.strftime('%Y-%m-%d %H:%M:%S')))
        self._eventlog_widget.setItem(0,1,QTableWidgetItem(msg.event_type))
        self._eventlog_widget.setItem(0,2,QTableWidgetItem(msg.task_name))
        self._eventlog_widget.setItem(0,3,QTableWidgetItem(msg.task_type))
        self._eventlog_widget.setItem(0,4,QTableWidgetItem(msg.event_text))
        self._eventlog_widget.setItem(0,5,QTableWidgetItem(msg.task_id))

    def _init_widgets(self):
        self._status_widget = QTableWidget()
        self._status_widget.setColumnCount(3)
        self._status_widget.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self._status_widget.setHorizontalHeaderItem(1, QTableWidgetItem("Type"))
        self._status_widget.setHorizontalHeaderItem(2, QTableWidgetItem("Timeout Left"))

        self._param_widget = QTableWidget()
        self._param_widget.setColumnCount(2)
        self._param_widget.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self._param_widget.setHorizontalHeaderItem(1, QTableWidgetItem("Value"))
        self._param_widget.cellChanged.connect(self.send_param_change)

        self._eventlog_widget = QTableWidget()
        self._eventlog_widget.setColumnCount(6)
        self._eventlog_widget.setRowCount(30)
        self._eventlog_widget.setHorizontalHeaderItem(0, QTableWidgetItem("Time"))
        self._eventlog_widget.setHorizontalHeaderItem(1, QTableWidgetItem("Event"))
        self._eventlog_widget.setHorizontalHeaderItem(2, QTableWidgetItem("Task Name"))
        self._eventlog_widget.setHorizontalHeaderItem(3, QTableWidgetItem("Task Type"))
        self._eventlog_widget.setHorizontalHeaderItem(4, QTableWidgetItem("Text"))
        self._eventlog_widget.setHorizontalHeaderItem(5, QTableWidgetItem("Task UUID"))

        self._widget = QWidget()
        self._topLayout = QHBoxLayout(self._widget)
        self._layout = QVBoxLayout()
        self._layout.addWidget(self._status_widget)
        self._layout.addWidget(self._param_widget)

        self._event_layout = QHBoxLayout()
        self._layout.addLayout(self._event_layout)

        self._event_label = QLabel("Event:")
        self._event_box = QLineEdit("/fault/leak")
        self._event_layout.addWidget(self._event_label)
        self._event_layout.addWidget(self._event_box)
        self._event_box.editingFinished.connect(self.send_event)

        self._topLayout.addLayout(self._layout)
        self._topLayout.addWidget(self._eventlog_widget)

    def send_param_change(self, row, col):
        name = self._param_widget.item(row, 0).text()
        value = self._param_widget.item(row, 1).text()
        self._param_widget.clearSelection()

        msg = MxUpdateSharedParamsRequest()
        msg.requested.header.stamp = rospy.Time.now()
        msg.requested.values.append(KeyString())
        msg.requested.values[0].key = name
        msg.requested.values[0].value = value
        print "\n\n%s = %s\n\n" % (name, value)
        try:
            self._parameter_service.call(msg)
        except rospy.service.ServiceException:
            print 'Could not set parameter %s!, out of range?' % name

    def send_event(self):
        text = self._event_box.text()
        print 'Sending \"%s\"' % text.strip()

        msg = MxEvent()
        msg.header.stamp = rospy.Time.now()
        msg.eventid = text.strip()
        self._event_publisher.publish(msg)

    def save_settings(self, plugin_settings, instance_settings):
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        pass
