/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/20/19.
//

#ifndef DS_MX_NAVG_MXPLANNER_H
#define DS_MX_NAVG_MXPLANNER_H


#include <navg/CorePluginInterface.h>

#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <QHBoxLayout>

#include <ros/ros.h>

#include <ds_mxcore/ds_mxcore.h>

#include <memory>

#include <QVariant>

#include "MxLayer.h"
#include "TaskView.h"
#include "TaskTreeView.h"

#include <ds_mx_msgs/MxMissionStatus.h>
#include <ds_mx_msgs/MissionDisplay.h>

Q_DECLARE_METATYPE(ds_mx_msgs::MissionDisplay)
Q_DECLARE_METATYPE(ds_mx_msgs::MxMissionStatus)

namespace navg {
namespace plugins {
namespace mx_planner {

// forward declaration
namespace Ui {
class MxPlanner;
}

// declare some underlying

class MxPlannerPlugin : public QWidget, public navg::CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "MxPlanner.json")

signals:
  void missionDisplayUpdated(ds_mx_msgs::MissionDisplay);
  void taskTreeDisplayUpdated(std::string dotcode);
  void missionStatusUpdated(ds_mx_msgs::MxMissionStatus);

public:
  explicit MxPlannerPlugin(QWidget* parent = nullptr);
  ~MxPlannerPlugin() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to acomms plugin.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

public slots:
  void openFileDialog();
  void openMissionFile(QString);
  void togglePlannerWindow();
  void validateMission();
  void selectTask(QUuid);
  void deselectTask();

private:
  std::unique_ptr<Ui::MxPlanner> ui;
  QPointer<dslmap::MapView> m_view;
  QPointer<QWidget> popout_window;
  QHBoxLayout* popout_layout;

  QPointer<MxLayer> m_mx_layer;
  QPointer<TaskView> task_view;
  QPointer<TaskTreeView> task_tree_view;

  std::shared_ptr<ds_mx::MxCompiler> mx_compiler;
  std::shared_ptr<ds_mx::TaskIndex> mx_tree_index;
  std::shared_ptr<ds_mx::MxRootTask> mx_task_tree;

  // ROS live update stuff;
  ros::NodeHandlePtr node_handle;
  std::unique_ptr<ros::Subscriber> mission_status_sub;
  void handleRosMissionStatus(ds_mx_msgs::MxMissionStatus status);

  void addMissionLayer();
  void updateMissionDisplay();
  void drawMission();
};

Q_DECLARE_LOGGING_CATEGORY(plugin_mx_planner)
} // mx_navg
} // plugins
} // ds_navg

#endif //DS_MX_NAVG_MXPLANNER_H
