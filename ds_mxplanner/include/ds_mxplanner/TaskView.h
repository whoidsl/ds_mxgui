/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/20/19.
//

#ifndef DS_MX_NAVG_TASKVIEW_H
#define DS_MX_NAVG_TASKVIEW_H

#include <QWidget>
#include <QAbstractTableModel>
#include <QAbstractItemModel>
#include <memory>
#include <ds_mxcore/ds_mxcore.h>
#include <QLoggingCategory>

namespace navg {
namespace plugins {
namespace mx_planner {

class ParameterCollectionModel : public QAbstractTableModel {
  Q_OBJECT

 public:
  ParameterCollectionModel(QObject *parent = nullptr);
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  void setTask(const std::shared_ptr<ds_mx::MxTask>& task);

 private:
  std::shared_ptr<ds_mx::MxTask> task;
};

// forward declaration
class EventTreeItem;

class EventCollectionModel : public QAbstractItemModel {
  Q_OBJECT

 public:
  EventCollectionModel(QObject *parent = nullptr);
  ~EventCollectionModel() override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  QModelIndex index(int row, int column, const QModelIndex &parent) const override;
  QModelIndex parent(const QModelIndex &parent = QModelIndex()) const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  void setTask(const std::shared_ptr<ds_mx::MxTask>& task);

 private:
  std::shared_ptr<ds_mx::MxTask> task;
  EventTreeItem* model_root;

  EventTreeItem* buildTreeModel(const ds_mx::EventHandlerCollection& events) const;
};

class SharedParameterModel : public QAbstractTableModel {
  Q_OBJECT

 public:
  SharedParameterModel(QObject *parent = nullptr);
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  void setSharedParams(std::shared_ptr<ds_mx::SharedParameterRegistry> registry);

 private:
  std::shared_ptr<ds_mx::SharedParameterRegistry> sharedParams;
};

// forward declaration
namespace Ui {
class TaskView;
}

class TaskView : public QWidget {
  Q_OBJECT;

 public:
  TaskView(QWidget* parent = nullptr);
  ~TaskView() override;

  void setTaskIndex(const std::shared_ptr<ds_mx::TaskIndex>& index);
  void setSharedParams(const std::shared_ptr<ds_mx::SharedParameterRegistry>& registry);

 public slots:
  void selectTask(QUuid id);
  void deselectTask();

 private:
  std::unique_ptr<Ui::TaskView> ui;
  std::shared_ptr<ds_mx::TaskIndex> taskIndex;
  std::shared_ptr<ds_mx::SharedParameterRegistry> sharedParams;
  ParameterCollectionModel parameterModel;
  EventCollectionModel eventModel;
  SharedParameterModel sharedParamModel;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_mx_taskview)

} // mkx_navg
} // plugins
} // ds_navg

#endif //DS_MX_NAVG_TASKVIEW_H
