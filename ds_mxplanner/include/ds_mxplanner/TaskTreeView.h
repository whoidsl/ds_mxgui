/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/20/19.
//

#ifndef DS_MX_NAVG_TASKTREEVIEW_H
#define DS_MX_NAVG_TASKTREEVIEW_H

#include <QWidget>
#include <QGraphicsItem>
#include <memory>
#include <QRectF>
#include <QPointF>
#include <QSizeF>
#include <QPen>
#include <QUuid>
#include <QWheelEvent>
#include <ds_mx_msgs/MxMissionStatus.h>
#include <ds_mxcore/ds_mxcore.h>
#include <gvc.h>
#include <QLoggingCategory>
#include <QtWidgets/QGraphicsView>
#include <boost/optional.hpp>

namespace navg {
namespace plugins {
namespace mx_planner {

// forward declaration
Q_DECLARE_LOGGING_CATEGORY(plugin_mx_tasktree)

class TaskTreeViewGraphicsView : public QGraphicsView {
  Q_OBJECT;
 public:
  TaskTreeViewGraphicsView(QWidget* parent=nullptr);
  void wheelEvent(QWheelEvent* event) override;
  void mouseMoveEvent(QMouseEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;
  void mouseReleaseEvent(QMouseEvent* event) override;

 signals:
  void scenePointClicked(QPointF scenePt);

 private:
  bool _pan;
  int _panStartX, _panStartY;
};

class TaskTreeView : public QWidget {
  Q_OBJECT;
  Q_DISABLE_COPY(TaskTreeView);

 public:
  TaskTreeView(QWidget* parent = nullptr);
  ~TaskTreeView() override;

  void setTaskIndex(const std::shared_ptr<ds_mx::TaskIndex>& index);

 public slots:
  //void updateTree(ds_mx_msgs::TaskTreeGraph);
  void updateTreeDot(std::string dotcode);
  void highlightCurrentTask(ds_mx_msgs::MxMissionStatus status);
  void resetColors();
  void selectTask(QUuid id);
  void deselectTask();
  void clearScene();
  void sceneClicked(QPointF scenePt);

  void setDefaultPen(QPen pen);
  void setActivePen(QPen pen);
  void setHighlightPen(QPen pen);

 signals:
  void taskClicked(QUuid id);
  void taskSelected(QUuid id);
  void clearTaskSelection();
  void wheelEvent(QWheelEvent* event);


 private:
  // graphviz overhead
  GVC_t* graphviz_context;
  Agraph_t* graphviz_graph;

  // underlying scene & supporting cast
  QGraphicsScene* scene;
  TaskTreeViewGraphicsView* view;
  QPen defaultPen;  // default pen for normal tasks
  QPen activePen;   // currently-active tasks
  QPen selectedPen; // selected task

  // index into graphics items
  std::map<QUuid, QAbstractGraphicsShapeItem*> tasks;
  std::map<QUuid, std::vector<QGraphicsItem*> > task_groups;

  boost::optional<QUuid> selectedTask;
  std::shared_ptr<ds_mx::TaskIndex> taskIndex;

  void updateGraphLayout(const std::string& dotcode);
  void updateGraphicsItems();
};

} // mkx_navg
} // plugins
} // ds_navg

#endif //DS_MX_NAVG_TASKTREEVIEW_H
