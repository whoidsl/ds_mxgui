/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/21/19.
//

#ifndef DS_MX_MXLAYER_H
#define DS_MX_MXLAYER_H

#include <QPaintEvent>
#include <QPen>
#include <QStyleOptionGraphicsItem>
#include <QtCore>
#include <dslmap/DslMap.h>
#include <dslmap/MapLayer.h>
#include <dslmap/MarkerItem.h>
#include <dslmap/Proj.h>

#include <ds_mx_msgs/MissionDisplay.h>
#include <ds_mxcore/ds_mxcore.h>

namespace navg {
namespace plugins {
namespace mx_planner {

class MxLayerPrivate;

class MxLayer : public dslmap::MapLayer {
  Q_OBJECT
  Q_DECLARE_PRIVATE(MxLayer)

 public:
  explicit MxLayer(QGraphicsItem* parent = Q_NULLPTR);
  ~MxLayer() override;

  void showPropertiesDialog() override;
  int type() const override
  {
    return dslmap::MapItemType::UserType + 1000;
  }
  void setName(const QString& name_) noexcept override;
  QMenu* contextMenu() override;
  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;
  static MxLayer* fromSettings(
    QSettings* settings);

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget = 0) override;

  void setTaskIndex(const std::shared_ptr<ds_mx::TaskIndex>& index);

 public slots:
  void updateMission(ds_mx_msgs::MissionDisplay);
  void updateBoundingRect();
  void drawMission();
  void setProjection(std::shared_ptr<const dslmap::Proj> proj) override;
  void changePen(const QString& role_name, const QPen& newpen);
  void setSelectedTask(QUuid taskid);
  void clearSelectedTask();

 private:
  MxLayerPrivate* d_ptr;

  void setupMenu();
  void savePenSettings(QPen& pen_ptr, QSettings& settings, QString name);
  QPen loadPenSettings(QSettings& settings, QString key);
};

Q_DECLARE_LOGGING_CATEGORY(plugin_mx_layer)

} // namespace mx_planner
} // namespace plugins
} // namespace navg

#endif //DS_MX_MXLAYER_H
