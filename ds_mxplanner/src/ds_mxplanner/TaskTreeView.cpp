/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/20/19.
//

#include "ds_mxplanner/TaskTreeView.h"
#include "ds_mxplanner/uuid_conversions.h"
#include "ui_TaskTreeView.h"
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <QtGui/QtGui>
#include <QScrollBar>

namespace navg {
namespace plugins {
namespace mx_planner {

/// Graphviz defines some things in points, some things in inches.
/// We use the Points-Per-Inch (PPI) to convert.
const static double GRAPHVIZ_PPI = 72.0;

QByteArray gv_load_raw(void* obj, const std::string& attrname, bool& ok) {
  // This FREAKING library didn't use a const pointer like it should have,
  // because they're idiots.  So make a copy and then const-cast.
  std::string attrcpy(attrname);
  char* raw = agget(obj, const_cast<char*>(attrcpy.c_str()));
  if (raw == nullptr) {
    ok = false;
    return QByteArray();
  }
  return QByteArray(raw, -1);
}

QString gv_load_string(void* obj, const std::string& attrname, bool& ok) {
  return QString(gv_load_raw(obj, attrname, ok));
}

qreal gv_load_number(void* obj, const std::string& attrname, bool& ok) {
  return gv_load_raw(obj, attrname, ok).toDouble(&ok);
}

QPointF gv_parse_point(const QByteArray& arr, bool& ok) {
  QByteArrayList parts = arr.split(',');

  if (parts.size() < 2) {
    ok = false;
    return QPointF();
  }

  bool ok_x=true, ok_y=true;
  QPointF ret(parts[0].toDouble(&ok_x), -parts[1].toDouble(&ok_y));
  if (!(ok_x && ok_y)) {
    ok = false;
  }
  return ret;
}

QPointF gv_load_point(void* obj, const std::string& attrname, bool& ok) {
  QByteArray raw = gv_load_raw(obj, attrname, ok);
  if (raw.isEmpty()) {
    ok = false;
    return QPointF();
  }
  return gv_parse_point(raw, ok);
}

QGraphicsPathItem* gv_load_spline(void* obj, const std::string& attrname, bool& ok) {
  boost::optional<QPointF> start_pt;
  boost::optional<QPointF> end_pt;
  ok = true;

  QByteArray raw = gv_load_raw(obj, attrname, ok);
  QByteArrayList raw_parts = raw.split(' ');

  // based on:
  // https://github.com/ros-visualization/qt_gui_core/blob/kinetic-devel/qt_dotgraph/src/qt_dotgraph/edge_item.py
  // and:
  // http://www.graphviz.org/doc/info/attrs.html#k:splineType
  if (raw_parts.front().startsWith("e,")) {
    raw_parts.front().remove(0, 2);
    end_pt = gv_parse_point(raw_parts.front(), ok);
    raw_parts.pop_front();
  }

  if (raw_parts.front().startsWith("s,")) {
    raw_parts.front().remove(0, 2);
    start_pt = gv_parse_point(raw_parts.front(), ok);
    raw_parts.pop_front();
  }

  // parse first point
  QPointF first_pt = gv_parse_point(raw_parts.front(), ok);
  raw_parts.pop_front();
  QPainterPath path(first_pt);

  QPointF pt1, pt2, pt3;
  while (raw_parts.size() > 2) {
    pt1 = gv_parse_point(raw_parts.front(), ok);
    raw_parts.pop_front();

    pt2 = gv_parse_point(raw_parts.front(), ok);
    raw_parts.pop_front();

    pt3 = gv_parse_point(raw_parts.front(), ok);
    raw_parts.pop_front();

    path.cubicTo(pt1, pt2, pt3);
  }

  // if there's an end point, add it to the path
  if (end_pt) {
    path.lineTo(*end_pt);
  }

  return new QGraphicsPathItem(path);
}

Q_LOGGING_CATEGORY(plugin_mx_tasktree, "navg.plugins.mx.tasktreeview")

// ////////////////////////////////////////////////////////////////////////// //
// TaskTreeViewGraphicsView
// ////////////////////////////////////////////////////////////////////////// //

TaskTreeViewGraphicsView::TaskTreeViewGraphicsView(QWidget *parent)
: QGraphicsView(parent) {
  _pan = false;
  _panStartX = 0;
  _panStartY = 0;
}

void TaskTreeViewGraphicsView::wheelEvent(QWheelEvent* event) {

  // copied from:
  // https://stackoverflow.com/questions/19113532/qgraphicsview-zooming-in-and-out-under-mouse-position-using-mouse-wheel
  //if (event->modifiers() & Qt::ControlModifier) {
  // do a wheel-based zoom about the cursor position
  double angle = event->angleDelta().y();
  double factor = qPow(1.0015, angle);

  auto targetViewportPos = event->pos();
  auto targetScenePos =  mapToScene(event->pos());

  scale(factor, factor);
  centerOn(targetScenePos);

  QPointF deltaViewportPos = targetViewportPos - QPointF(viewport()->width() / 2.0,
                                                         viewport()->height() / 2.0);
  QPointF viewportCenter = mapFromScene(targetScenePos) - deltaViewportPos;
  centerOn(mapToScene(viewportCenter.toPoint()));
  //}
}

void TaskTreeViewGraphicsView::mouseMoveEvent(QMouseEvent *event) {
  if (_pan) {
    horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - _panStartX));
    verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - _panStartY));
    _panStartX = event->x();
    _panStartY = event->y();
    event->accept();
    return;
  }
  event->ignore();
}

void TaskTreeViewGraphicsView::mousePressEvent(QMouseEvent *event) {
  // scroll
  if (event->button() == Qt::RightButton) {
    _pan = true;
    _panStartX = event->x();
    _panStartY = event->y();
    setCursor(Qt::ClosedHandCursor);
    event->accept();
    return;
  }
  if (event->button() == Qt::LeftButton) {
    QPointF scenePos = mapToScene(event->pos());
    qDebug(plugin_mx_tasktree) <<"Emitting position: " <<scenePos;
    emit scenePointClicked(scenePos);
    event->accept();
    return;
  }
  event->ignore();
}

void TaskTreeViewGraphicsView::mouseReleaseEvent(QMouseEvent *event) {

  if (event->button() == Qt::RightButton) {
    _pan = false;
    setCursor(Qt::ArrowCursor);
    event->accept();
    return;
  }
  event->ignore();
}

// ////////////////////////////////////////////////////////////////////////// //
// TaskTreeView
// ////////////////////////////////////////////////////////////////////////// //
TaskTreeView::TaskTreeView(QWidget* parent): QWidget(parent), scene(new QGraphicsScene(this)),
view(new TaskTreeViewGraphicsView(this)) {
  graphviz_context = gvContext();
  graphviz_graph = nullptr;

  defaultPen = QPen(Qt::white);
  defaultPen.setWidthF(2.0);
  activePen = QPen(Qt::green);
  activePen.setWidthF(2.0);
  selectedPen = QPen(Qt::red);
  selectedPen.setWidthF(2.0);

  view->setScene(scene);
  setLayout(new QGridLayout());
  layout()->addWidget(view);

  connect(view, &TaskTreeViewGraphicsView::scenePointClicked, this, &TaskTreeView::sceneClicked);
}

TaskTreeView::~TaskTreeView() {
  clearScene();

  if (graphviz_context) {
    gvFreeContext(graphviz_context);
  }
}

void TaskTreeView::clearScene() {
  if (graphviz_graph) {
    gvFreeLayout(graphviz_context, graphviz_graph);
    agclose(graphviz_graph);
    graphviz_graph = nullptr;
  }

  // clear the index
  tasks.clear();
  task_groups.clear();

  // remove all QGraphics Items from the scene
  for (QGraphicsItem* item : scene->items()) {
    scene->removeItem(item);
    delete item;
  }
}

void setItemColor(QGraphicsItem* item, QPen pen) {
  // do a dyanmic cast and check to rule out text items
  QAbstractGraphicsShapeItem* shape = dynamic_cast<QAbstractGraphicsShapeItem*>(item);
  if (shape) {
    shape->setPen(pen);
    return;
  }

  QGraphicsTextItem* text = dynamic_cast<QGraphicsTextItem*>(item);
  if (text) {
    text->setDefaultTextColor(pen.color());
    return;
  }
}

void TaskTreeView::highlightCurrentTask(ds_mx_msgs::MxMissionStatus status) {

  resetColors();

  for (auto activeTask : status.active_tasks) {
    QUuid activeUuid = uuid_conv::rosToQt(activeTask.uuid);

    //std::map<QUuid, std::vector<QGraphicsItem*> >::iterator group = task_groups.find(activeUuid);
    auto group = task_groups.find(activeUuid);
    if (group == task_groups.end()) {
      continue;
    }

    for (auto item : group->second) {
      setItemColor(item, activePen);
    }
  }

  if (selectedTask) {
    selectTask(*selectedTask);
  }
}

void TaskTreeView::resetColors() {
  for (QGraphicsItem* item : scene->items()) {
    // do a dyanmic cast and check to rule out text items
    setItemColor(item, defaultPen);
  }
}

void TaskTreeView::setTaskIndex(const std::shared_ptr<ds_mx::TaskIndex> &index) {
  taskIndex = index;
}

void TaskTreeView::selectTask(QUuid id) {
  selectedTask = id;

  auto taskGraphicsItemIter = tasks.find(id);
  if (taskGraphicsItemIter != tasks.end()) {
    taskGraphicsItemIter->second->setPen(selectedPen);
  }
}

void TaskTreeView::deselectTask() {
  if (selectedTask) {
    auto taskGraphicsItemIter = tasks.find(*selectedTask);
    if (taskGraphicsItemIter != tasks.end()) {
      // if its "active path" then it'll get fixed soon anyway
      taskGraphicsItemIter->second->setPen(defaultPen);
    }
  }

  selectedTask.reset();
}

void TaskTreeView::setDefaultPen(QPen pen) {
  defaultPen = pen;
}

void TaskTreeView::setActivePen(QPen pen) {
  activePen = pen;
}

void TaskTreeView::setHighlightPen(QPen pen) {
  selectedPen = pen;
}

void TaskTreeView::updateTreeDot(std::string dotcode) {
  // cleanup any old graph
  qDebug(plugin_mx_tasktree) << "Cleaning out old graphviz graph layout";
  clearScene();

  // layout a new one
  qDebug(plugin_mx_tasktree) << "Loading dotcode...";
  updateGraphLayout(dotcode);
  if (graphviz_graph == nullptr) {
    qWarning(plugin_mx_tasktree) <<"Unable to load dotcode!";
    return;
  }

  updateGraphicsItems();
}

void TaskTreeView::updateGraphLayout(const std::string &dotcode) {
  graphviz_graph = agmemread(dotcode.c_str());
  if (graphviz_graph == nullptr) {
    qDebug(plugin_mx_tasktree) <<"Unable to load dotcode!";
    return;
  }
  qDebug(plugin_mx_tasktree) <<"Laying out tasktree...";
  gvLayout(graphviz_context, graphviz_graph, "dot");
  qDebug(plugin_mx_tasktree) <<"TaskTree Layout Complete!";
  gvRenderFilename(graphviz_context, graphviz_graph, "dot", nullptr); // this adds positions/size
  qDebug(plugin_mx_tasktree) <<"TaskTree test render complete!";

}

void TaskTreeView::updateGraphicsItems() {

  // iterate through the ndoes
  for (Agnode_t* task=agfstnode(graphviz_graph); task; task = agnxtnode(graphviz_graph, task)) {

    // parse the UUID
    std::string raw_uuid_str(agnameof(task));
    std::replace(raw_uuid_str.begin(), raw_uuid_str.end(), '_', '-');
    QUuid uuid(QString::fromStdString(raw_uuid_str.substr(1)));

    //qDebug(plugin_mx_tasktree) << "TASK: " <<uuid;

    // parse the node
    bool ok=true;
    QPointF task_pos = gv_load_point(task, "pos", ok);
    qreal size_w = gv_load_number(task, "width", ok);
    qreal size_h = gv_load_number(task, "height", ok);
    QSizeF task_size(size_w*GRAPHVIZ_PPI, size_h*GRAPHVIZ_PPI);
    QString task_shape = gv_load_string(task, "shape", ok);
    QString task_label = gv_load_string(task, "label", ok);
    if (!ok) {
      qWarning(plugin_mx_tasktree) <<"Unable to parse Task Node for UUID " <<uuid;
    }

    // actually draw stuff
    QAbstractGraphicsShapeItem *item;
    QRectF task_rect(task_pos.x() - task_size.width() / 2.0,
                     task_pos.y() - task_size.height() / 2.0,
                     task_size.width(), task_size.height());

    if (task_shape == "ellipse") {
      item = new QGraphicsEllipseItem(task_rect);
    } else if (task_shape == "box") {
      item = new QGraphicsRectItem(task_rect);
    } else {
      item = new QGraphicsRectItem(task_rect);
      qWarning(plugin_mx_tasktree) << "Task " << uuid << " has unknown shape " << task_shape;
    }
    item->setPen(defaultPen);

    QGraphicsTextItem* label_item = new QGraphicsTextItem(task_label);
    QFont font = label_item->font();
    font.setPointSize(12);
    font.setFamily("Times New Roman");
    label_item->setFont(font);
    label_item->setDefaultTextColor(defaultPen.color());
    QRectF bb = label_item->boundingRect();
    label_item->setPos(task_pos.x() - bb.width()/2.0, task_pos.y() - bb.height()/2.0);

    // add to the scene
    scene->addItem(item);
    tasks[uuid] = item;
    std::vector<QGraphicsItem*> group_items;
    group_items.push_back(item);
    group_items.push_back(label_item);

    scene->addItem(label_item);

    // now render edges pointing TO this node
    // to do this, we have to iterate through a list of all edges...
    for (Agedge_t* edge=agfstedge(graphviz_graph, task); edge; edge = agnxtedge(graphviz_graph, edge, task)) {
      // and look for one that points to this task
      if (aghead(edge) == task) {
        bool ok=true;
        //qDebug(plugin_mx_tasktree) <<"Adding edge from " <<agnameof(aghead(edge)) << " to " <<agnameof(agtail(edge));

        // parse the edge itself
        QGraphicsPathItem* edge_item = gv_load_spline(edge, "pos", ok);
        if (!ok) {
          qWarning(plugin_mx_tasktree) <<"Task " <<uuid <<" unable to load edge path!";
          break;
        }
        edge_item->setPen(defaultPen);
        scene->addItem(edge_item);
        group_items.push_back(edge_item);

        // parse teh label
        ok = true;
        QPointF edge_pos = gv_load_point(edge, "lp", ok);
        QString edge_label = gv_load_string(edge, "label", ok);
        if (!ok) {
          qWarning(plugin_mx_tasktree) <<"Task " <<uuid <<" unable to load edge label!";
          break;
        }

        // add the label
        QGraphicsTextItem* edge_label_item = new QGraphicsTextItem(edge_label);
        QFont font = edge_label_item->font();
        font.setPointSize(10);
        font.setFamily("Times New Roman");
        edge_label_item->setFont(font);
        edge_label_item->setDefaultTextColor(defaultPen.color());
        QRectF bb = edge_label_item->boundingRect();
        edge_label_item->setPos(edge_pos.x() - bb.width()/2.0, edge_pos.y() - bb.height()/2.0);
        group_items.push_back(edge_label_item);
        scene->addItem(edge_label_item);
      } // if correct edge
    } // for all edges
    task_groups[uuid] = group_items;
  } // for each task
}

void TaskTreeView::sceneClicked(QPointF scenePt) {
  for (auto task : tasks) {
    if (task.second->boundingRect().contains(scenePt)) {
      qDebug(plugin_mx_tasktree) <<"Task selected: " <<task.first;
      emit taskClicked(task.first);
      if (selectedTask && *selectedTask == task.first) {
        emit clearTaskSelection();
      } else {
        emit taskSelected(task.first);
      }
    }
  }
}

} // namespace mx_planner
} // namespace plugins
} // namespace navg