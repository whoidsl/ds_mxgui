/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/20/19.
//

#include "ds_mxplanner/MxPlanner.h"
#include "ui_MxPlanner.h"

#include <memory>
#include <QSettings>
#include <QFileDialog>
#include <QFileInfo>
#include <navg/NavGMapScene.h>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/Proj.h>

#include <boost/uuid/uuid_generators.hpp>

namespace navg {
namespace plugins {
namespace mx_planner {

Q_LOGGING_CATEGORY(plugin_mx_planner, "navg.plugins.mx.planner")

MxPlannerPlugin::MxPlannerPlugin(QWidget* parent) : QWidget(parent),
                                                    ui(std::unique_ptr<Ui::MxPlanner>(new Ui::MxPlanner)),
                                                    mx_compiler(ds_mx::MxCompiler::create()),
                                                    mx_tree_index(new ds_mx::TaskIndex)
{
  qDebug(plugin_mx_planner) <<"Starting MX NAVG plugin";
  qRegisterMetaType<ds_mx_msgs::MissionDisplay>();
  qRegisterMetaType<ds_mx_msgs::MxMissionStatus>();

  // setup our own window (easy part)
  ui->setupUi(this);
  ui->filenameLabel->setText(tr(""));

  // now build our popup window
  popout_window = new QWidget(this, Qt::Window);
  popout_layout = new QHBoxLayout(popout_window);

  task_tree_view = new TaskTreeView(popout_window);
  popout_layout->addWidget(task_tree_view);

  task_view = new TaskView(popout_window);
  popout_layout->addWidget(task_view);

  m_mx_layer = new MxLayer();

  connect(ui->filenameButton, &QPushButton::released, this, &MxPlannerPlugin::openFileDialog);
  connect(ui->openPlanner, &QPushButton::released, this, &MxPlannerPlugin::togglePlannerWindow);
  connect(ui->validateButton, &QPushButton::released, this, &MxPlannerPlugin::validateMission);
  connect(this, &MxPlannerPlugin::taskTreeDisplayUpdated, task_tree_view, &TaskTreeView::updateTreeDot);
  // updates only happen if a RosBackend plugin is also loaded
  connect(this, &MxPlannerPlugin::missionStatusUpdated, task_tree_view, &TaskTreeView::highlightCurrentTask);

  connect(task_tree_view, &TaskTreeView::taskSelected, this, &MxPlannerPlugin::selectTask);
  connect(task_tree_view, &TaskTreeView::clearTaskSelection, this, &MxPlannerPlugin::deselectTask);

  popout_window->show();
}

MxPlannerPlugin::~MxPlannerPlugin() {

}

void
MxPlannerPlugin::loadSettings(QSettings* settings) {
  QString mission_file = settings->value("mission_file", "").toString();

  if (! mission_file.isEmpty() ) {
    openMissionFile(mission_file);
    ui->filenameLabel->setText(mission_file);
  }
  m_mx_layer->loadSettings(*settings);
}

void
MxPlannerPlugin::saveSettings(QSettings& settings) {
  QString mission_file = ui->filenameLabel->text();
  if (! mission_file.isEmpty() ) {
    settings.setValue("mission_file", mission_file);
  }

  m_mx_layer->saveSettings(settings);

}

void
MxPlannerPlugin::handleRosMissionStatus(ds_mx_msgs::MxMissionStatus status) {
  emit missionStatusUpdated(status);
}

// This plugin connects to acomms plugin.
void
MxPlannerPlugin::connectPlugin(const QObject* plugin,  const QJsonObject& metadata) {
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "dsros_Backend") {
    qDebug(plugin_mx_planner) << "Connecting to Live Mission Updates via ROS...";

    node_handle.reset(new ros::NodeHandle());
    std::string topicname = "/sentry/mission/mx_state";
    mission_status_sub.reset(new ros::Subscriber(node_handle->subscribe(topicname, 10,
                                                    &MxPlannerPlugin::handleRosMissionStatus, this)));
  }
}

void
MxPlannerPlugin::setMapView(QPointer<dslmap::MapView> view) {
  m_view = view;

  if (mx_task_tree) {
    qDebug(plugin_mx_planner) <<"MapView connected! Mission already loaded, adding mission layer...";
    addMissionLayer();
    updateMissionDisplay();
  }

  connect(m_view->mapScene(), &dslmap::MapScene::projectionChanged,
      [this](QString){
    this->drawMission();
  });
}

QList<QAction*>
MxPlannerPlugin::pluginMenuActions() {
  return {};
}

void
MxPlannerPlugin::openFileDialog() {
  QString filename = QFileDialog::getOpenFileName(this,
      tr("Open Mission"),
      QDir::currentPath(),
      "Mission files (*.json) ;; All files (*.*)");

  if (filename.isNull()) {
    qDebug(plugin_mx_planner) <<"TaskView got empty filename from dialog!";
    return;
  }

  QFileInfo checker(filename);
  if (checker.exists() && checker.isFile()) {
    qDebug(plugin_mx_planner) <<"Request to open mission file " <<filename;
    openMissionFile(filename);
  } else {
    qDebug(plugin_mx_planner) <<"Dialog returned filename " <<filename <<" which does not exist or is not a file!";
  }
}

void
MxPlannerPlugin::openMissionFile(QString filename) {
  qDebug(plugin_mx_planner) << "MxPlanner opening mission file " << filename;

  QFileInfo missionfile(filename);
  if (!missionfile.exists()) {
    qCritical(plugin_mx_planner) << "File " << filename << " does not exist!";
    return;
  }
  if (!missionfile.isFile()) {
    qCritical(plugin_mx_planner) << "File " << filename << " is not a file!";
    return;
  }

  qDebug(plugin_mx_planner) << "MxPlanner compiling mission " << filename;

  // Actually load & compile the mission
  ds_mx::eventlog_disable(); // no events from planner, just live missions
  Json::Value json_root = ds_mx::parseFile(filename.toStdString());
  mx_task_tree = mx_compiler->compile(json_root);
  mx_tree_index->setRoot(mx_task_tree);
  m_mx_layer->setTaskIndex(mx_tree_index);
  task_tree_view->setTaskIndex(mx_tree_index);
  task_view->setTaskIndex(mx_tree_index);
  task_view->setSharedParams(mx_compiler->getSharedParams());

  // update views
  validateMission();

  // update the map display
  if (m_view != nullptr) {
    qDebug(plugin_mx_planner) <<"Mission loaded: MapView already defined, adding mission layer...";
    addMissionLayer();
  }
  updateMissionDisplay();

  // update the TaskTree display
  emit taskTreeDisplayUpdated(mx_task_tree->toDot());

  ui->filenameLabel->setText(missionfile.canonicalFilePath());
}

void
MxPlannerPlugin::updateMissionDisplay() {
  ds_nav_msgs::NavState launchState = mx_task_tree->getLaunchState();
  ds_mx_msgs::MissionDisplay display;
  mx_task_tree->getDisplay(launchState, display);

  emit missionDisplayUpdated(display);
}

void
MxPlannerPlugin::addMissionLayer() {
  // Get a copy of the scene
  if (m_view == nullptr) {
    qDebug(plugin_mx_planner) << "m_view is NULL!, returning...";
    return;
  }
  auto my_scene = qobject_cast<dslmap::MapScene *>(m_view->mapScene());
  Q_ASSERT(my_scene != nullptr);

  // Save our current settings
  QSettings settings;
  if (m_mx_layer) {
    m_mx_layer->saveSettings(settings);
  }

  // shutdown the existing layer
  if (m_mx_layer) {
    my_scene->removeLayer(m_mx_layer->id());
  }

  // replace it
  m_mx_layer = MxLayer::fromSettings(&settings);
  m_mx_layer->setName(QStringLiteral("mission"));
  m_mx_layer->setTaskIndex(mx_tree_index);
  // we need an initial mission to draw in order to successfully add the mission
  connect(this, &MxPlannerPlugin::missionDisplayUpdated, m_mx_layer, &MxLayer::updateMission);
  updateMissionDisplay();

  // add to scene by re-drawing... handles an issue with no coordinate system issues
  //my_scene->addLayer(m_mx_layer);
  drawMission(); // automatically adds layer
}

void
MxPlannerPlugin::drawMission() {
  qDebug(plugin_mx_planner) <<"Re-drawing scene...";
  m_mx_layer->drawMission();

  // now we have to RE-ADD the mission layer because there's a good chance it simply refused to add
  if (m_view == nullptr) {
    qDebug(plugin_mx_planner) << "m_view is NULL!, returning...";
    return;
  }
  auto my_scene = qobject_cast<dslmap::MapScene *>(m_view->mapScene());
  Q_ASSERT(my_scene != nullptr);

  qDebug(plugin_mx_planner) <<"Layer projection: " <<m_mx_layer->projection()->isLatLon();
  qDebug(plugin_mx_planner) <<"Bounding box: " <<m_mx_layer->boundingRect();

  if (my_scene->layer(m_mx_layer->id()) == nullptr) {
    qDebug(plugin_mx_planner) <<"Re-adding layer...";
    my_scene->addLayer(m_mx_layer);
  }
}

void
MxPlannerPlugin::togglePlannerWindow() {
  if (popout_window->isVisible()) {
    popout_window->hide();
  } else {
    popout_window->show();
  }
}

void
MxPlannerPlugin::validateMission() {

  if (! mx_task_tree) {
    ui->validLabel->setText(tr("NO MISSION"));
    ui->validLabel->setStyleSheet("QLabel { background-color : red; color: white; }");
    return;
  }

  bool rc = mx_task_tree->validate();

  if (rc) {
    ui->validLabel->setText(tr("Valid"));
    ui->validLabel->setStyleSheet("QLabel { background-color : green; color: black; }");
  } else {
    ui->validLabel->setText(tr("INVALID"));
    ui->validLabel->setStyleSheet("QLabel { background-color : red; color: white; }");
  }
}

void
MxPlannerPlugin::selectTask(QUuid uuid) {
  // map layer
  m_mx_layer->setSelectedTask(uuid);

  // task tree:
  task_tree_view->selectTask(uuid);

  // individual task view:
  task_view->selectTask(uuid);
}

void
MxPlannerPlugin::deselectTask() {
  // map layer
  m_mx_layer->clearSelectedTask();

  // task tree:
  task_tree_view->deselectTask();

  // individual task view
  task_view->deselectTask();
}

} // namespace mx_planner
} // namespace plugins
} // namespace navg