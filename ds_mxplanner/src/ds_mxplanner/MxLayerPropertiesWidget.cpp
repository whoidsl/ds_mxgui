/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/24/19.
//

#include "ds_mxplanner/MxLayerPropertiesWidget.h"

namespace navg {
namespace plugins {
namespace mx_planner {

MxLayerPropertiesWidget::MxLayerPropertiesWidget(QWidget *parent) : QWidget(parent) {
  layout = new QHBoxLayout(this);
}

MxLayerPropertiesWidget::~MxLayerPropertiesWidget() = default;

void MxLayerPropertiesWidget::setPens(const std::map<QString, QPen> &pens) {

  // clear all widgets from the layout
  QLayoutItem* item;
  while ( ( item = layout->takeAt(0) ) != Q_NULLPTR ) {
    delete item->widget();
    delete item;
  }

  // this vector is now full of dangling pointers, so clear it
  widgets.clear();

  // instantiate new widgets
  for (const std::pair<QString, QPen>& entry : pens) {

    // build the widget
    auto widget = new dslmap::PenPropertiesWidget(this);
    widget->setVisible(true);
    widget->setName(entry.first);
    widget->setPen(entry.second);

    // wire up a connection
    QString name = entry.first;
    connect(widget, &dslmap::PenPropertiesWidget::penChanged, [this, name](const QPen& pen) {
      this->emitChanged(name, pen);
    });

    // add to bookeeping
    layout->addWidget(widget);
    widgets.push_back(widget);
  }
}

void MxLayerPropertiesWidget::emitChanged(const QString &name, const QPen &pen) {
  emit penChanged(name, pen);
}

} // namespace mx_planner
} // namespace plugins
} // namespace navg