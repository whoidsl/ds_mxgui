/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/20/19.
//

#include <memory>
#include <QtWidgets/QFileDialog>
#include <QtCore/QtCore>
#include "ds_mxplanner/TaskView.h"
#include "ui_TaskView.h"
#include "ds_mxplanner/uuid_conversions.h"

namespace navg {
namespace plugins {
namespace mx_planner {

Q_LOGGING_CATEGORY(plugin_mx_taskview, "navg.plugins.mx.planner");

// ///////////////////////////////////////////////////////////////////////// //
// ParameterCollectionModel
// ///////////////////////////////////////////////////////////////////////// //
ParameterCollectionModel::ParameterCollectionModel(QObject *parent) : QAbstractTableModel(parent) {
  // do nothing
}

void ParameterCollectionModel::setTask(const std::shared_ptr<ds_mx::MxTask>& _task) {
  beginResetModel();
  task = _task;
  endResetModel();
}

int ParameterCollectionModel::rowCount(const QModelIndex &parent) const {
  if (! task) {
    return 0;
  }

  return task->getParameters().size();
}

int ParameterCollectionModel::columnCount(const QModelIndex &parent) const {
  return 4;
}

QVariant ParameterCollectionModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
    switch (section) {
      case 0:
        return QString("Type");
      case 1:
        return QString("Name");
      case 2:
        return QString("Value");
      case 3:
        return QString("Dynamic");
    }
  }
  return QVariant();
}

QVariant ParameterCollectionModel::data(const QModelIndex &index, int role) const {
  int row = index.row();
  int col = index.column();
  const auto& params = task->getParameters();

  switch (role) {
    case Qt::DisplayRole:
      if (col == 0) {
        return QString::fromStdString(params[row]->TypeName());
      } else if (col == 1) {
        return QString::fromStdString(params[row]->getName());
      } else if (col == 2) {
        return QString::fromStdString(params[row]->ValueString());
      }
      break;

    case Qt::CheckStateRole:
      if (col == 3) {
        if (params[row]->isDynamic()) {
          return Qt::Checked;
        } else {
          return Qt::Unchecked;
        }
      }
  }
  return QVariant();
}

// ///////////////////////////////////////////////////////////////////////// //
// EventCollectionModel
// ///////////////////////////////////////////////////////////////////////// //

// so for the record... trees suck.

class EventTreeItem {
 public:

  EventTreeItem(QString _label, EventTreeItem* _parent) {
    m_label = _label;
    m_parent = _parent;
  }
  virtual ~EventTreeItem() {
    for (auto child : m_children) {
      delete child;
    }
    m_children.clear();
  }

  QString label() const {
    return m_label;
  }

  int row() const {
    return m_parent->m_children.indexOf(const_cast<EventTreeItem*>(this));
  }

  EventTreeItem* parentItem() {
    return m_parent;
  }

  EventTreeItem* child(int i) const {
    return m_children[i];
  }

  void addChild(EventTreeItem* _child) {
    m_children.append(_child);
  }

  int childCount() const {
    return m_children.size();
  }

 private:
  QString m_label;
  EventTreeItem* m_parent;
  QList<EventTreeItem*> m_children;


};

EventCollectionModel::EventCollectionModel(QObject *parent): QAbstractItemModel(parent), model_root(nullptr) {

}

EventCollectionModel::~EventCollectionModel() {
  if (model_root) {
    delete model_root;
    model_root = nullptr;
  }
}

QVariant EventCollectionModel::data(const QModelIndex &index, int role) const {

  if (!index.isValid()) {
    return QVariant();
  }

  EventTreeItem* item = static_cast<EventTreeItem*>(index.internalPointer());
  if (role == Qt::DisplayRole) {
    return item->label();
  }

  return QVariant();
}

Qt::ItemFlags EventCollectionModel::flags(const QModelIndex &index) const {
  if (!index.isValid()) {
    return 0;
  }
  return QAbstractItemModel::flags(index);
}

QVariant EventCollectionModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
    switch (section) {
      case 0:
        return QString("Event");
    }
  }
  return QVariant();
}

QModelIndex EventCollectionModel::index(int row, int column, const QModelIndex &parent) const {
  if (!hasIndex(row, column, parent) || model_root == nullptr) {
    return QModelIndex();
  }

  // set the parent item pointer correctly
  EventTreeItem* parentItem;
  if (parent.isValid()) {
    parentItem = static_cast<EventTreeItem*>(parent.internalPointer());
  } else {
    parentItem = model_root;
  }

  EventTreeItem* childItem = parentItem->child(row);
  if (childItem) {
    return createIndex(row, column, childItem);
  }
  return QModelIndex();
}

QModelIndex EventCollectionModel::parent(const QModelIndex &index) const {
  if (!index.isValid()) {
    return QModelIndex();
  }

  EventTreeItem* childItem = static_cast<EventTreeItem*>(index.internalPointer());
  EventTreeItem* parentItem = childItem->parentItem();

  if (parentItem == model_root) {
    return QModelIndex();
  }

  return createIndex(parentItem->row(), 0, parentItem);
}

int EventCollectionModel::rowCount(const QModelIndex &parent) const {
  if (model_root == nullptr) {
    return 0;
  }

  if (parent.column() > 0) {
    return 0;
  }

  EventTreeItem* parentItem;
  if (parent.isValid()) {
    parentItem = static_cast<EventTreeItem *>(parent.internalPointer());
  } else {
    parentItem = model_root;
  }

  return parentItem->childCount();
}

int EventCollectionModel::columnCount(const QModelIndex &parent) const {
  return 1;
}


void EventCollectionModel::setTask(const std::shared_ptr<ds_mx::MxTask>& _task) {
  beginResetModel();
  task = _task;
  delete model_root;
  model_root = buildTreeModel(task->getEvents());
  endResetModel();
}

EventTreeItem* EventCollectionModel::buildTreeModel(const ds_mx::EventHandlerCollection& events) const {

  if (! task) {
    return nullptr;
  }

  EventTreeItem* ret = new EventTreeItem(QString(""), nullptr);
  qDebug(plugin_mx_taskview) <<"Creating new event tree...";

  for (const auto& handler : task->getEvents() ){
    EventTreeItem* handlerItem = new EventTreeItem(QString::fromStdString(handler->getName()), ret);
    ret->addChild(handlerItem);
    qDebug(plugin_mx_taskview) <<"\tAdding handler named" <<handlerItem->label();

    for (const auto& pattern : handler->getMatcherStrings()) {
      handlerItem->addChild(new EventTreeItem(QString::fromStdString(pattern), handlerItem));
      qDebug(plugin_mx_taskview) <<"\t\tAdding pattern" <<QString::fromStdString(pattern);
    }
  }

  return ret;
}

// ///////////////////////////////////////////////////////////////////////// //
// SharedParameterModel
// ///////////////////////////////////////////////////////////////////////// //
SharedParameterModel::SharedParameterModel(QObject* parent) : QAbstractTableModel(parent) {}

int SharedParameterModel::rowCount(const QModelIndex &parent) const {
  if (!sharedParams) {
    return 0;
  }
  return sharedParams->collection().size();
}

int SharedParameterModel::columnCount(const QModelIndex &parent) const {
  return 6;

}

QVariant SharedParameterModel::data(const QModelIndex &index, int role) const {
  int row = index.row();
  int col = index.column();
  const auto& param = sharedParams->collection()[row];

  switch (role) {
    case Qt::DisplayRole:
      if (col == 0) {
        return QString::fromStdString(param->TypeName());
      } else if (col == 1) {
        return QString::fromStdString(param->getName());
      } else if (col == 2) {
        return QString::fromStdString(param->GuiString(ds_mx::RoleValue));
      } else if (col == 3) {
        return QString::fromStdString(param->GuiString(ds_mx::RoleMin));
      } else if (col == 4) {
        return QString::fromStdString(param->GuiString(ds_mx::RoleMax));
      }
      break;
    case Qt::CheckStateRole:
      if (col == 5) {
        if (param->isDynamic()) {
          return Qt::Checked;
        } else {
          return Qt::Unchecked;
        }
      }

  }
  return QVariant();
}

QVariant SharedParameterModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
    switch (section) {
      case 0:
        return QString("Type");
      case 1:
        return QString("Name");
      case 2:
        return QString("Initial");
      case 3:
        return QString("Min");
      case 4:
        return QString("Max");
      case 5:
        return QString("Dynamic");
    }
  }
  return QVariant();
}

void SharedParameterModel::setSharedParams(std::shared_ptr<ds_mx::SharedParameterRegistry> registry) {
  beginResetModel();
  sharedParams = registry;
  endResetModel();
}


// ///////////////////////////////////////////////////////////////////////// //
// TaskView
// ///////////////////////////////////////////////////////////////////////// //
Q_DECLARE_LOGGING_CATEGORY(plugin_mx_planner);

TaskView::TaskView(QWidget* parent): QWidget(parent), ui(std::unique_ptr<Ui::TaskView>(new Ui::TaskView)),
parameterModel(this) {
  ui->setupUi(this);
  ui->parameterTableView->setModel(&parameterModel);
  ui->eventTreeView->setModel(&eventModel);
  ui->sharedParameterTableView->setModel(&sharedParamModel);
}

TaskView::~TaskView() = default;

void TaskView::setTaskIndex(const std::shared_ptr<ds_mx::TaskIndex>& index) {
  taskIndex = index;
}

void TaskView::setSharedParams(const std::shared_ptr<ds_mx::SharedParameterRegistry> &registry) {
  sharedParams = registry;
  sharedParamModel.setSharedParams(registry);
}

void TaskView::selectTask(QUuid id) {
  auto task = taskIndex->operator[](uuid_conv::qtToBoost(id));
  if (! task) {
    qWarning(plugin_mx_taskview) <<"No task with ID " <<id;
    deselectTask();
    return;
  }

  qDebug(plugin_mx_taskview) <<"Task set to ID " <<id;
  parameterModel.setTask(task);
  eventModel.setTask(task);
  ui->eventTreeView->expandAll();
}

void TaskView::deselectTask() {
  qDebug(plugin_mx_taskview) <<"Task set to NULL";
  parameterModel.setTask(std::shared_ptr<ds_mx::MxTask>());
  eventModel.setTask(std::shared_ptr<ds_mx::MxTask>());
}

} // namespace mx_planner
} // namespace plugins
} // namespace navg