/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/21/19.
//

#include "ds_mxplanner/MxLayer.h"
#include "ds_mxplanner/MxLayerPropertiesWidget.h"
#include "ds_mxplanner/uuid_conversions.h"

#include <map>
#include <QPen>
#include <QMenu>
#include <QRect>
#include <QGraphicsScene>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <ds_mx_msgs/MissionDisplay.h>
#include <boost/uuid/uuid.hpp>

namespace navg {
namespace plugins {
namespace mx_planner {

Q_LOGGING_CATEGORY(plugin_mx_layer, "navg.plugins.mx.layer")

class MxLayerPrivate
{
 public:
  explicit MxLayerPrivate() : unknown_role_pen(Qt::white, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin),
  m_menu(new QMenu), m_properties(new MxLayerPropertiesWidget) {
    // always add a "selected" pen
    mission_role_pens["selected"] = QPen(Qt::red);
  }

  ~MxLayerPrivate() = default;

  ds_mx_msgs::MissionDisplay mission;

  std::map<QString, QPen> mission_role_pens;
  QPen unknown_role_pen;
  QRectF m_bounding_rect;
  QScopedPointer<QMenu> m_menu;
  QScopedPointer<MxLayerPropertiesWidget> m_properties;
  boost::optional<boost::uuids::uuid> selectedTask;
  std::shared_ptr<ds_mx::TaskIndex> taskIndex;

  // we need to store our Graphics Shape Items so they don't
  // go anywhere
  std::vector<QGraphicsItem*> graphics;
};

MxLayer::MxLayer(QGraphicsItem *parent) : dslmap::MapLayer(parent), d_ptr(new MxLayerPrivate) {
  Q_D(MxLayer);
  setupMenu();

  setProjection(dslmap::Proj::fromDefinition(PROJ_WGS84_DEFINITION));
  connect(d->m_properties.data(), &MxLayerPropertiesWidget::penChanged, this, &MxLayer::changePen);
}

MxLayer::~MxLayer() = default;

void
MxLayer::showPropertiesDialog() {
  Q_D(MxLayer);
  d->m_properties->show();
}

void
MxLayer::setName(const QString& name_) noexcept {
  if (name_ == name()) {
    return;
  }

  setObjectName(name_);
  Q_D(MxLayer);
  d->m_menu->setTitle(name_);

}

void
MxLayer::setupMenu() {
  Q_D(MxLayer);
  d->m_menu->setTitle(name());
  auto action = d->m_menu->addAction(QStringLiteral("Visible"));

  action->setCheckable(true);
  action->setChecked(isVisible());
  connect(action, &QAction::toggled, this,
          [=](bool visible) { setVisible(visible); });
  d->m_menu->addAction(action);

  action = d->m_menu->addAction(QStringLiteral("Properties"));
  connect(action, &QAction::triggered, this, [=]() { showPropertiesDialog(); });
  d->m_menu->addAction(action);
}

QMenu*
MxLayer::contextMenu() {
  Q_D(MxLayer);
  return d->m_menu.data();
}

bool
MxLayer::saveSettings(QSettings& settings) {
  Q_D(MxLayer);

  settings.setValue("name", name());

  settings.beginGroup("pens");
  for (std::pair<QString, QPen> role_pen : d->mission_role_pens) {
    savePenSettings(role_pen.second, settings, role_pen.first);
  }
  settings.endGroup();
  return true;
}

bool
MxLayer::loadSettings(QSettings& settings) {
  Q_D(MxLayer);
  qDebug(plugin_mx_layer) <<"Loading settings for MxLayer...";

  setName(settings.value("name", "mission").toString());
  settings.beginGroup("pens");
  for (const QString& name : settings.childGroups()) {
    qDebug(plugin_mx_layer) <<"Loading pen for role " <<name;
    QPen newpen = loadPenSettings(settings, name);
    d->mission_role_pens[name] = newpen;
  }
  settings.endGroup();

  return true;
}

void
MxLayer::savePenSettings(QPen& pen_ptr, QSettings& settings,
                                      QString prefix) {
  settings.beginGroup(prefix);
  settings.setValue("width", pen_ptr.width());
  settings.setValue("color", pen_ptr.color().name());
  settings.setValue("style", QMetaEnum::fromType<Qt::PenStyle>().valueToKey(pen_ptr.style()));
  settings.endGroup();
}

QPen
MxLayer::loadPenSettings(QSettings& settings, QString prefix) {
  QPen pen;

  bool ok = false;

  settings.beginGroup(prefix);
  pen.setWidth(settings.value("width").toInt());

  auto value = settings.value("color");
  if (value.isValid()) {
    const auto color = QColor{ value.toString() };
    if (color.isValid()) {
      pen.setColor(color);
    }
  }

  value = settings.value("style");
  if (value.isValid()) {
    const auto key = value.toString().toStdString();
    const auto style =
      QMetaEnum::fromType<Qt::PenStyle>().keyToValue(key.data(), &ok);
    if (ok) {
      pen.setStyle(static_cast<Qt::PenStyle>(style));
    }
  }

  settings.endGroup();

  return pen;
}

MxLayer*
MxLayer::fromSettings(QSettings* settings) {
  auto layer = new MxLayer();
  layer->loadSettings(*settings);
  return layer;

}

QRectF
MxLayer::boundingRect() const {
  const Q_D(MxLayer);
  return d->m_bounding_rect;
}

void
MxLayer::updateMission(ds_mx_msgs::MissionDisplay _mission) {
  Q_D(MxLayer);
  d->mission = _mission;

  drawMission();
  d->m_properties->setPens(d->mission_role_pens);
}

void
MxLayer::setProjection(std::shared_ptr<const dslmap::Proj> proj) {
  qDebug(plugin_mx_layer) <<"Setting projection to " <<proj->definition();
  // do whatever the base class does
  dslmap::MapLayer::setProjection(proj);

  // re-draw the mission onto the correct projection
  drawMission();
}

void
MxLayer::changePen(const QString &role_name, const QPen &newpen) {
  qDebug(plugin_mx_layer) <<"Changing pen for role " <<role_name;

  Q_D(MxLayer);
  d->mission_role_pens[role_name] = newpen;
  drawMission();
}

void
MxLayer::setSelectedTask(QUuid taskid) {
  Q_D(MxLayer);
  qDebug(plugin_mx_layer) <<"Setting selected task id to " <<taskid;
  d->selectedTask = uuid_conv::qtToBoost(taskid);
  drawMission();
}

void
MxLayer::clearSelectedTask() {
  Q_D(MxLayer);
  d->selectedTask.reset();
  drawMission();

}

void
MxLayer::setTaskIndex(const std::shared_ptr<ds_mx::TaskIndex> &index) {
  Q_D(MxLayer);
  d->taskIndex = index;
}

/// Draw the mission; convert mission display elements to actual
/// QGraphicsItems
void
MxLayer::drawMission() {
  Q_D(MxLayer);

  qDebug(plugin_mx_layer) <<"Drawing mission...";

  auto proj = projectionPtr();
  if (!proj) {
    qDebug(plugin_mx_layer) <<"PSYCH! No projection, aborting...";
    return;
  }
  typedef boost::geometry::model::d2::point_xy<double> point_type;

  // delete all the old graphics
  for (auto i : d->graphics) {
    QGraphicsScene* s = i->scene();
    if (s != nullptr) {
      //qDebug(plugin_mx_layer) <<"Removing item...";
      s->removeItem(i);
    }
    delete i;
  }
  d->graphics.clear();

  // we'll need the actual task object for the currently-selected task
  std::shared_ptr<ds_mx::MxTask> selectedTask;
  if (d->taskIndex && d->selectedTask) {
    selectedTask = d->taskIndex->operator[](*(d->selectedTask));
  }

  QPointF label_point;
  // iterate through the elements
  for (const auto& element : d->mission.elements) {
    // get the pen to draw this mission element with
    QString role = QString::fromStdString(element.role);
    if (d->mission_role_pens.find(role) == d->mission_role_pens.end()) {
      // Previously unseen mission role! Add it...
      d->mission_role_pens[role] = d->unknown_role_pen;
    }
    QPen pen = d->mission_role_pens[role];

    if (selectedTask && selectedTask->subtreeContains(uuid_conv::rosToBoost(element.task_uuid))) {
      //qDebug(plugin_mx_layer) <<"Setting pen for task ID " <<uuid_conv::rosToQt(element.task_uuid);
      pen.setColor(d->mission_role_pens["selected"].color());
    }

    // Convert the geometry to the correct QGraphicsItem
    QString wkt = QString::fromStdString(element.wellknowntext);
    if (wkt.startsWith("LINESTRING", Qt::CaseInsensitive)) {
      QPainterPath path;

      boost::geometry::model::linestring<point_type> linestring;
      boost::geometry::read_wkt(element.wellknowntext, linestring);

      bool started = false;
      for (const point_type& pt : linestring) {
        qreal lon = pt.x();
        qreal lat = pt.y();

        // transform to layer projection
        dslmap::ProjWGS84::getInstance().transformTo(*proj, 1, &lon, &lat);

        const auto qt_pt = mapFromScene(lon, lat);
        if (started) {
          path.lineTo(qt_pt);
        } else {
          path.moveTo(qt_pt);
          started = true;
        }
      }

      auto item = new QGraphicsPathItem(path, this);
      item->setPen(pen);
      item->setVisible(true);
      d->graphics.push_back(item);

      // save where to draw a label, just in case we need one
      label_point = path.boundingRect().center();

    } else if (wkt.startsWith("POINT", Qt::CaseInsensitive)) {
      point_type wkt_pt;
      boost::geometry::read_wkt(element.wellknowntext, wkt_pt);


      qreal lon = wkt_pt.x();
      qreal lat = wkt_pt.y();

      dslmap::ProjWGS84::getInstance().transformTo(*proj, 1, &lon, &lat);

      const auto qt_pt = mapFromScene(lon, lat);

      qreal size = pen.widthF();
      auto item = new QGraphicsEllipseItem(qt_pt.x()-size/2, qt_pt.y()-size/2, size, size, this);
      pen.setBrush(QBrush(pen.color(), Qt::SolidPattern));
      item->setPen(pen);
      item->setVisible(true);
      d->graphics.push_back(item);

      // save where to draw a label, just in case we need one
      label_point = qt_pt;

    } else if (wkt.startsWith("POLYGON", Qt::CaseInsensitive)) {
      /*
      QPolygonF polygon;

      boost::geometry::model::polygon<point_type> wkt_poly;
      boost::geometry::read_wkt(element.wellknowntext, wkt_poly);

     }
      for (const point_type& pt : wkt_poly) {

      }

      auto item = new QGraphicsPolygonItem(this);
       */
      // TODO
    } else {
      qCritical(plugin_mx_layer) <<"Unknown Well-known Text " <<wkt;
      continue;
    }

  } // for each mission element
  updateBoundingRect();
}

void
MxLayer::updateBoundingRect() {
  Q_D(MxLayer);
  QRectF bounding{};

  for (auto item : d->graphics) {
    bounding |= mapFromItem(item, item->boundingRect()).boundingRect();
  }
  prepareGeometryChange();
  d->m_bounding_rect = bounding;

  //qDebug(plugin_mx_layer) <<"Bounding rect: " <<bounding;
}

void
MxLayer::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) {

}

} // namespace mx_planner
} // namespace plugins
} // namespace navg
