# Planner capabilities

A properly designed mission planner should address the following needs:

* How to insert primitives in a map in the appropriate location and in the appropriate sequence. A primitive could be a line, a lawnmower, a maggy spin, etc...
* How to assign or modify properties to primitives. A property, for example, is what payload should be running and what are the payload parameters. The selected properties should be visually inferrable from the user by just observing the map.
* How to validate a mission. This should run the same validator that the actual mission executive will run.
* How to transfer mission to vehicle. Ideally, can we avoid using command line push/pull which can be subject to errors fro inexperienced users? Can we serialize and send the json file directly to the vehicle with the push of a button?
* How to monitor mission progress. This must happen both via ethernet or acoustic modem.
* How to manipulate the mission while the mission is running. This could be via ethernet or via acoustic modem.