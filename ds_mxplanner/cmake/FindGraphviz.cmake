# Find Proj
# ~~~~~~~~~
# Copyright (c) 2007, Martin Dobias <wonder.sk at gmail.com>
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#
# CMake module to search for Proj library
#
# If it's found it sets PROJ_FOUND to TRUE
# and following variables are set:
#    PROJ_INCLUDE_DIR
#    PROJ_LIBRARY

# FIND_PATH and FIND_LIBRARY normally search standard locations
# before the specified paths. To search non-standard paths first,
# FIND_* is invoked first with specified paths and NO_DEFAULT_PATH
# and then again with no specified paths to search the default
# locations. When an earlier FIND_* succeeds, subsequent FIND_*s
# searching for the same item do nothing. 

# GraphViz DOES provide pkgconfig (note: you'll need graphviz-dev)!
include(FindPkgConfig)
#pkg_check_modules(Graphviz REQUIRED libcgraph)
pkg_check_modules(Graphviz REQUIRED libgvc)

#message(STATUS "Found:       ${Graphviz_FOUND}")
#message(STATUS "Libraries:   ${Graphviz_LIBRARIES}")
#message(STATUS "LibDirs:     ${Graphviz_LIBRARY_DIRS}")
#message(STATUS "LDFLAGS:     ${Graphviz_LDFLAGS}")
#message(STATUS "INCLUDE_DIRS ${Graphviz_INCLUDE_DIRS}")
#message(STATUS "CFLAGS ${Graphviz_CFLAGS}")
